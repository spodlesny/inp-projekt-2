-- cpu.vhd: Simple 8-bit CPU (BrainLove interpreter)
-- Copyright (C) 2016 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): DOPLNIT
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
 port (
   CLK   : in std_logic;  -- hodinovy signal
   RESET : in std_logic;  -- asynchronni reset procesoru
   EN    : in std_logic;  -- povoleni cinnosti procesoru
 
   -- synchronni pamet ROM
   CODE_ADDR : out std_logic_vector(11 downto 0); -- adresa do pameti
   CODE_DATA : in std_logic_vector(7 downto 0);   -- CODE_DATA <- rom[CODE_ADDR] pokud CODE_EN='1'
   CODE_EN   : out std_logic;                     -- povoleni cinnosti
   
   -- synchronni pamet RAM
   DATA_ADDR  : out std_logic_vector(9 downto 0); -- adresa do pameti
   DATA_WDATA : out std_logic_vector(7 downto 0); -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
   DATA_RDATA : in std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
   DATA_RDWR  : out std_logic;                    -- cteni (1) / zapis (0)
   DATA_EN    : out std_logic;                    -- povoleni cinnosti
   
   -- vstupni port
   IN_DATA   : in std_logic_vector(7 downto 0);   -- IN_DATA <- stav klavesnice pokud IN_VLD='1' a IN_REQ='1'
   IN_VLD    : in std_logic;                      -- data platna
   IN_REQ    : out std_logic;                     -- pozadavek na vstup data
   
   -- vystupni port
   OUT_DATA : out  std_logic_vector(7 downto 0);  -- zapisovana data
   OUT_BUSY : in std_logic;                       -- LCD je zaneprazdnen (1), nelze zapisovat
   OUT_WE   : out std_logic                       -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'
 );
end cpu;


-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is

-- konecny automat - stavy ToDo: premenovat na FSM_state
type FSM_states_t is (load_instruction, eval_instruction, load_value, eval_value, wat_for_value, wat_for_instruction, save_value, right_sqb, left_sqb, skip_while, write_on_output, read_input, increment_PC, halt, save_tmp, decrement_PC, was_last, load_tmp);
signal FSM_states : FSM_states_t;

-- addressing data in RAM = 1KB // PTR in scheme
signal data_ptr : std_logic_vector(9 downto 0) := (others => '0');

-- brace yourself :) //pocitanie zatvoriek - CNT in scheme
signal braces : std_logic_vector (11 downto 0) := (others => '0'); -- max 2^12 = 4094 because I can

-- indexing data in ROM / 2^12 = 4 KB ROM // PC in scheme
signal program_counter : std_logic_vector(11 downto 0) := (others => '0');

-- for incrementing and decrementing values switch - 1 is incrementation, 0 is decrementation
signal val_mod : std_logic := '0';

-- TMP register
signal tmp_reg : std_logic_vector(7 downto 0) := (others => '0');

-- tmp value from load
signal tmp_val : std_logic_vector(7 downto 0) := (others => '0');

-- last bracket 0 = left, 1 = right
signal last_bracket : std_logic := '0';
begin

    process (CLK, RESET)
    begin
	    if RESET /= '0' then
	        braces <= (others => '0');
	        program_counter <= (others => '0');
	        data_ptr <= (others => '0');
	        FSM_states <= load_instruction;

	    elsif rising_edge(CLK) and EN = '1' then
		    CODE_EN <= '0';
            DATA_RDWR <= '0';
            DATA_EN <= '0';
            IN_REQ <= '0';
		    OUT_WE <= '0';

            case FSM_states is
		
		        when load_instruction => 
		            CODE_ADDR <= program_counter; 
		            CODE_EN <= '1';
		            FSM_states <= wat_for_instruction;
		            
		        when wat_for_instruction =>
		            if braces = 0 then
		            	FSM_states <= eval_instruction;
                    else
                        FSM_states <= skip_while;
                    end if;

                when  load_value =>
                    DATA_ADDR <= data_ptr;
                    DATA_RDWR <= '1';
                    DATA_EN <= '1';
                    FSM_states <= wat_for_value;
                    
                when wat_for_value =>
                    case CODE_DATA is
                        when X"2B" =>  -- znak +
                            val_mod <= '1';
                            FSM_states <= eval_value;
                        when X"2D" =>  -- znak -
                            val_mod <= '0';
                            FSM_states <= eval_value;
                        when X"2E" =>  -- znak .
                            FSM_states <= write_on_output;
                        when X"5B" =>  -- znak [
                            FSM_states <= left_sqb;
                        when X"5D" =>  -- znak ]
                        	FSM_states <= right_sqb;
                        when X"24" => -- znak $
                        	FSM_states <= load_tmp;
                        when others =>
                            null;
                    end case;

                
                when eval_instruction => 
                    case CODE_DATA is
                        when X"00" => -- halt
                            FSM_states <= halt;
                        when X"2b" => -- +
                            FSM_states <= load_value;
                        when X"2C" => -- ,
                            FSM_states <= read_input;
                        when X"2E" => -- .
                            FSM_states <= load_value;
                        when X"2D" => -- -
                            FSM_states <= load_value;
                        when X"3C" => -- <
                        	data_ptr <= data_ptr - 1;
                            FSM_states <= increment_PC;
                        when X"3E" => -- >
                        	data_ptr <= data_ptr + 1;
                            FSM_states <= increment_PC;
                        when X"5B" => -- [
                            FSM_states <= load_value;
                        when X"5D" => -- ]
                        	FSM_states <= load_value;
                        when X"24" => -- $
                        	FSM_states <= load_value;
                        WHEN x"21" => -- !
                        	FSM_states <= save_tmp;
                        when others =>
                            FSM_states <= increment_PC;
                    end case;
                    
                when eval_value =>
                    if val_mod = '1' then
                        tmp_val <= DATA_RDATA + 1;
                    end if;

                    if val_mod = '0' then
                        tmp_val <= DATA_RDATA - 1;
                    end if;

                    FSM_states <= save_value;

                when save_value => 
                    DATA_ADDR <= data_ptr;
                    DATA_RDWR <= '0';
                    DATA_EN <= '1';
                    DATA_WDATA <= tmp_val; 
                    FSM_states <= increment_PC;

                when write_on_output =>
                    if OUT_BUSY = '0' then
                        OUT_DATA <= DATA_RDATA;
                        OUT_WE <= '1';
                        FSM_states <= increment_PC;
                    else
                        FSM_states <= write_on_output;
                    end if;
                
                when read_input => 
                    IN_REQ <= '1';
                    if IN_VLD = '1' then
                        tmp_val <= IN_DATA;
                        FSM_states <= save_value;
                    else
                        FSM_states <= read_input;
                    end if;
                    
                when load_tmp =>
                    tmp_reg <= DATA_RDATA;
					FSM_states <= increment_PC;
                    
                when save_tmp =>
                	DATA_ADDR <= data_ptr;
                    DATA_RDWR <= '0';
                    DATA_EN <= '1';
                    DATA_WDATA <= tmp_reg; 
                    FSM_states <= increment_PC;

                when increment_PC =>
                    program_counter <= program_counter + 1;
                    FSM_states <= load_instruction;
                    
                when decrement_PC =>
                    program_counter <= program_counter - 1;
                    FSM_states <= load_instruction;
                    
                when left_sqb =>
                	if DATA_RDATA = 0 then
                		braces <= braces + 1;
                	end if;
                	last_bracket <= '0';
                	FSM_states <= increment_PC;
                	
               	
               	when right_sqb => 
               		if DATA_RDATA = 0 then
               			FSM_states <= increment_PC;
               		else
               			braces <= braces + 1;
               			FSM_states <= decrement_PC;
               		end if;
               		last_bracket <= '1';
               		
               	when skip_while => 
               		if DATA_RDATA = 0 and last_bracket = '0' then 
               			case CODE_DATA is
	               			when X"5B" => -- [
	               				braces <= braces + 1;
	                        when X"5D" => -- ]
	                        	braces <= braces - 1;
	                        when others =>
	                        	null;
            			end case;
            			FSM_states <= increment_PC;
            		elsif DATA_RDATA /= 0 and last_bracket = '1' then
            			case CODE_DATA is
	               			when X"5B" => -- [
	               				braces <= braces - 1;
	                        when X"5D" => -- ]
	                        	braces <= braces + 1;
	                        when others =>
	                        	null;
            			end case;
            			FSM_states <= was_last;
        			end if;
                	  
				when was_last =>
					if braces = 0 then
            				FSM_states <= increment_PC;
        			else
        				FSM_states <= decrement_PC;
        			end if;
										                     	
                when others =>
                	null;
                	

            end case;
    	end if;
    end process;
end behavioral;
